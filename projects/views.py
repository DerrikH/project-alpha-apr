from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project, Company
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"project_list": projects}
    return render(request, "projects/list.html", context)


@login_required
def show_projects(request, id):
    tasks = get_object_or_404(Project, id=id)
    context = {"tasks_list": tasks}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)

@login_required
def show_companies(request):
    company_id = request.GET.get("company_id")
    if company_id and company_id != "-1":
        companies = Company.objects.filter(id=company_id)
    else:
        companies = Company.objects.all()
    dropdown_companies = Company.objects.all()
    context = {
        "show_companies": companies,
        "dropdown_companies": dropdown_companies
    }
    return render(request, "projects/company.html", context)
