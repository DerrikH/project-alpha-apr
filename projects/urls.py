from django.urls import path
from projects.views import (
    project_list, show_projects, create_project, show_companies,
)

urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>", show_projects, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("company/", show_companies, name="show_companies"),
    ]
