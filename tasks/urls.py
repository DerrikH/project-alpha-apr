from django.urls import path
from tasks.views import create_task, show_my_tasks, note_tasks, view_notes, edit_task


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("create_note/<int:id>", note_tasks, name="create_note"),
    path("view_notes/<int:id>", view_notes, name="view_notes"),
    path("edit/<int:id>", edit_task, name="edit_task"),
]
