from django.shortcuts import render, redirect
from tasks.forms import TaskForm, NoteForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.models import Project


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    project = Project.objects.filter(owner=request.user)
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": my_tasks,
        "project": project,
    }
    return render(request, "tasks/mine.html", context)

@login_required
def note_tasks(request, id):
    note = Task.objects.get(id=id)
    if request.method == "POST":
        form = NoteForm(request.POST, instance=note)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = NoteForm(instance=note)
    context = {
        "note": note,
        "form": form,
    }
    return render(request, "tasks/create_notes.html", context)

@login_required
def view_notes(request, id):
    task = Task.objects.get(id=id)
    context = {
        "note": task.notes,
    }
    return render(request, "tasks/view_notes.html", context)

@login_required
def edit_task(request, id):
    task = Task.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(instance=task)
    context = {
        "task": task,
        "form": form,
    }
    return render(request, "tasks/edit.html", context)
